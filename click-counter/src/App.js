import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Click Counter Demo</h1>
        </header>
        <div onClick={this.props.onClick}>
          <p>
          This has been clicked {this.props.clicks} times
          </p>
        </div>
      </div>
    );
  }
}

export default App;
